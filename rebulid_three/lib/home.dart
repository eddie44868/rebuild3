import 'package:flutter/material.dart';
import 'package:flutter_point_tab_bar/pointTabBar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rebulid_three/widget/product_list.dart';
import 'widget/special_list.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          toolbarHeight: 305,
          flexibleSpace: Column(
            children: [
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: const Color.fromARGB(255, 47, 46, 46),
                              borderRadius: BorderRadius.circular(13),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/category.png',
                                  width: 20,
                                  height: 20,
                                  color: Colors.grey,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: const Color.fromARGB(255, 47, 46, 46),
                              borderRadius: BorderRadius.circular(13),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/man.png',
                                  width: 25,
                                  height: 25,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Find the Best\ncoffee for you',
                        style: GoogleFonts.inter(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Flexible(
                            child: SizedBox(
                              height: 40,
                              width: 315,
                              child: TextFormField(
                                cursorColor: Colors.black,
                                decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(
                                        top: 11, bottom: 11),
                                    fillColor:
                                        const Color.fromARGB(255, 47, 46, 46),
                                    filled: true,
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(13),
                                      borderSide: BorderSide.none,
                                    ),
                                    hintText: "Search Your Coffee",
                                    hintStyle: GoogleFonts.inter(
                                        color: const Color.fromARGB(
                                            255, 118, 108, 108),
                                        fontSize: 14),
                                    prefixIcon: const Icon(
                                      Icons.search_rounded,
                                      color: Color.fromARGB(255, 118, 108, 108),
                                    )),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 24,
              ),
              TabBar(
                isScrollable: true,
                tabs: [
                  Tab(
                    child: Text(
                      'Cappuccino',
                      style: GoogleFonts.montserrat(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Espresso',
                      style:
                          GoogleFonts.montserrat(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Latte',
                      style:
                          GoogleFonts.montserrat(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Flat Coffee',
                      style: GoogleFonts.montserrat(
                          fontSize: 13, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
                indicator: const PointTabIndicator(
                  position: PointTabIndicatorPosition.bottom,
                  color: Color.fromARGB(255, 207, 131, 76),
                  insets: EdgeInsets.only(bottom: 6),
                ),
                labelColor: const Color.fromARGB(255, 207, 131, 76),
                unselectedLabelColor: const Color.fromARGB(255, 118, 108, 108),
              ),
            ],
          ),
        ),
        body: TabBarView(children: [
          Scaffold(
            backgroundColor: Colors.black,
            body: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: SafeArea(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15, top: 10),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Column(
                            children: [
                              Row(
                                children: [
                                  ProductList(
                                    imageUrl: 'assets/kopi8.jpg',
                                    text: 'Cappuccino',
                                    text2: 'With Oat Milk',
                                    rating: '4.5',
                                    price: '4.20',
                                    based: "Milk",
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  ProductList(
                                    imageUrl: 'assets/kopi6.jpg',
                                    text: 'Cappuccino',
                                    text2: 'With Chocolate',
                                    rating: '4.2',
                                    price: '3.14',
                                    based: "Chocolate",
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Special for you',
                              style: GoogleFonts.inter(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              )),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            SpecialList(
                              imageUrl: "assets/kopi4.jpg",
                              text: "5 Coffee Beans You\nMust Try!",
                              text2:
                                  'Lorem Ipsum ist ein einfacher Demo-\nText für die Print- und Schriftindustrie.\nLorem Ipsum ist in der Industrie bereits\nder Standard Demo-Text seit 1500,\nals ein unbekannter Schriftsteller eine.',
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: const [],
            ),
          ),
          Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: const [],
            ),
          ),
          Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: const [],
            ),
          ),
        ]),
      ),
    );
  }
}
