import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'home.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  int _selectedIndex = 0;
  final halaman = [
    const Home(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: halaman[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.black,
        items: <BottomNavigationBarItem>[
          const BottomNavigationBarItem(
            icon: Icon(
                Icons.home_filled,
                size: 25,
              ),
              label: ''
          ),
          const BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/shopping-bag.png',), size: 25,),
            label: ''
          ),
          const BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/heart1.png')),
            label: ''
          ),
          BottomNavigationBarItem(
            icon: Stack(
          children: const <Widget>[
            Icon(Icons.notifications),
            Positioned(
              top: 0.0,
              right: 0.0,
              child: Icon(Icons.brightness_1, size: 8.0, 
                color: Colors.redAccent),
            )]),
            label: ''
          ),
        ],
        selectedLabelStyle: const TextStyle(fontSize: 10, fontWeight: FontWeight.w900,),
        unselectedLabelStyle: GoogleFonts.inter(
          color: const Color(0xffCFCFCF),
          fontSize: 20,
        ),
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.orange,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
        
      ),
    );
  }
}
