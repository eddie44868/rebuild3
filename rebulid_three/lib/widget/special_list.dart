import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class SpecialList extends StatefulWidget {
  
  String imageUrl;
  String text;
  String text2;

  SpecialList({Key? key, required this.imageUrl, required this.text, required this.text2}) : super(key: key);

  @override
  State<SpecialList> createState() => _SpecialListState();
}

class _SpecialListState extends State<SpecialList> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 330,
          height: 140,
          decoration: BoxDecoration(
            color: const Color.fromARGB(223, 50, 49, 49),
            borderRadius: BorderRadius.circular(16),
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Image.asset(
                    widget.imageUrl,
                    width: 120,
                    height: 120,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.text,
                      style: GoogleFonts.inter(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                          widget.text2,
                          style: GoogleFonts.inter(
                              fontSize: 10, fontWeight: FontWeight.w500, color: Colors.white),
                          textAlign: TextAlign.justify,
                        ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
